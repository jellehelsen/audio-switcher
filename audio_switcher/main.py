#!/usr/bin/env python3

import subprocess

def parse_wpctl_status():
    """
    function to parse output of command "wpctl status" and return a dictionary of sinks with their id and name.
    """
    wpctl_output = str(subprocess.check_output("wpctl status", shell=True, encoding='utf-8'))

    # remove the ascii tree characters and return a list of lines
    lines = wpctl_output.replace("├", "").replace("─", "").replace("│", "").replace("└", "").splitlines()

    # get the index of the Sinks line as a starting point
    sinks_index = next(i for i, x in enumerate(lines) if "Sinks:" in x)

    # start by getting the lines after "Sinks:" and before the next blank line
    sinks = {}
    for line in lines[sinks_index +1:]:
        if not line.strip(): # break on the first empty line
            break
        sink = {
            "default": False,
        }
        name = line.strip().split("[vol:")[0].strip()
        if name.startswith("*"):
            name = name.replace("*", "").strip()
            sink["default"] = True
        (sink_id, name) = name.split(".")
        sink['id'] = sink_id
        sinks[name.strip()] = sink

    return sinks


def main():
    sinks = parse_wpctl_status()
    rofi_input = "\n".join(sinks.keys())
    rofi_cmd = subprocess.run(["rofi", "-dmenu", "-i"], input=bytes(rofi_input, encoding="utf-8"), capture_output=True)
    if rofi_cmd.returncode != 0:
        print("User canceled")
        return 0
    selected_name = rofi_cmd.stdout.decode().strip()
    if selected_name not in sinks:
        print(f"Sink {selected_name} not found!!")
        return 1
    wpctl = subprocess.run(["wpctl", "set-default", sinks[selected_name]["id"]])
    return wpctl.returncode
